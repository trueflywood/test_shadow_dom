import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'picture-card-s',
    templateUrl: './picture-card-s.component.html',
    styleUrls: ['./picture-card-s.component.scss'],
    encapsulation: ViewEncapsulation.ShadowDom
})
export class PictureCardSComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
