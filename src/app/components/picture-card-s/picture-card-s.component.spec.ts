import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PictureCardSComponent} from './picture-card-s.component';

describe('PictureCardSComponent', () => {
    let component: PictureCardSComponent;
    let fixture: ComponentFixture<PictureCardSComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PictureCardSComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PictureCardSComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
