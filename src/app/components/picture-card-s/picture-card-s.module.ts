import {Injector, NgModule}    from '@angular/core';
import {CommonModule}          from '@angular/common';
import {PictureCardSComponent} from './picture-card-s.component';
import {createCustomElement}   from '@angular/elements';

@NgModule({
    declarations: [PictureCardSComponent],
    imports: [
        CommonModule
    ],
    exports: [PictureCardSComponent]
})
export class PictureCardSModule {
    constructor(private injector: Injector) {}
    ngDoBootstrap() {
        const element = createCustomElement(PictureCardSComponent, {injector: this.injector});
        customElements.define('picture-card-s', element);
    }
}
