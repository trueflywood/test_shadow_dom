import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {PictureCardSModule} from './components/picture-card-s/picture-card-s.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PictureCardSModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
